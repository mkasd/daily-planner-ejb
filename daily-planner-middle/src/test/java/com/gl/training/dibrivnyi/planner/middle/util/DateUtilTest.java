package com.gl.training.dibrivnyi.planner.middle.util;

import com.gl.training.dibrivnyi.planner.middle.util.DateUtil;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Alexandr Dibrivnyi
 *         16.08.14
 *         22:33
 */
public class DateUtilTest {

    @Test
    public void testIsDayBefore_future_date() throws Exception {
        Calendar calendar = Calendar.getInstance();

        calendar.set(2014, Calendar.DECEMBER, 31);
        Date selectedDate = calendar.getTime();

        assertThat(DateUtil.isDayBefore(selectedDate)).isFalse();
    }

    @Test
    public void testIsDayBefore_past_date() throws Exception {
        Calendar calendar = Calendar.getInstance();

        calendar.set(2014, Calendar.JANUARY, 31);
        Date selectedDate = calendar.getTime();

        assertThat(DateUtil.isDayBefore(selectedDate));
    }

    @Test
    public void testIsDayBefore_current_date() {
        Date currentDate = new Date();

        assertThat(DateUtil.isDayBefore(currentDate)).isFalse();
    }
}
