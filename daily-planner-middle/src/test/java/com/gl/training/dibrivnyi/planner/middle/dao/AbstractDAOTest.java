package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.api.AbstractDaoImpl;
import com.gl.training.dibrivnyi.planner.middle.api.DataAccessObjectTestCase;
import com.gl.training.dibrivnyi.planner.middle.api.FakeEntity;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AbstractDAOTest extends DataAccessObjectTestCase<AbstractDaoImpl> {

    public AbstractDAOTest() {
        super(AbstractDaoImpl.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindOne_null_id() throws Exception {
        try {
            bean.findOne(null);
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo("id to load is required for loading");
            throw e;
        }
    }

    @Test
    public void testFindOne_no_such_id() throws Exception {
        FakeEntity fakeEntity = bean.findOne(123L);

        assertThat(fakeEntity).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSave_save_null() throws Exception {
        try {
            executeWithTransaction(new Action() {
                @Override
                public void execute() {
                    bean.save(null);
                }
            });
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo("attempt to create create event with null entity");
            throw e;
        }
    }

    @Ignore
    @Test
    public void testSave() throws Exception {
        final FakeEntity entity = new FakeEntity();
        entity.setFakeFirstField("f1");
        entity.setFakeSecondField("f2");
        final FakeEntity[] result = new FakeEntity[1];

        executeWithTransaction(new Action() {
            @Override
            public void execute() {
                result[0] = bean.save(entity);
            }
        });

        List<FakeEntity> entities = em.createQuery("select fe from FakeEntity fe", FakeEntity.class).getResultList();
        assertThat(result[0]).isSameAs(entity);
        assertThat(entities).hasSize(4);
    }

    @Test
    public void testSaveMultiple() throws Exception {

    }
}