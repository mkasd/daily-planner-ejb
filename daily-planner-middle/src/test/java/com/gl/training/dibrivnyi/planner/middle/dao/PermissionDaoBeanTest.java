package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.api.DataAccessObjectTestCase;
import com.gl.training.dibrivnyi.planner.middle.entity.Permission;
import com.gl.training.dibrivnyi.planner.middle.entity.Role;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PermissionDaoBeanTest extends DataAccessObjectTestCase<PermissionDaoBean> {

    public PermissionDaoBeanTest() {
        super(PermissionDaoBean.class);
    }

    @Test
    public void testFindUserPermissions_null_login() throws Exception {
        List<Permission> result = bean.findUserPermissions(null);

        assertThat(result).isEmpty();
    }

    @Test
    public void testFindUserPermission_user_has_not_permissions() throws Exception {
        List<Permission> result = bean.findUserPermissions("tuser1");

        assertThat(result).hasSize(0);
    }

    @Test
    public void testFindUserPermission() throws Exception {
        List<Permission> result = bean.findUserPermissions("mkasd");

        assertThat(result).hasSize(2).extracting("role").containsOnly(Role.ADMIN, Role.USER);
    }
}