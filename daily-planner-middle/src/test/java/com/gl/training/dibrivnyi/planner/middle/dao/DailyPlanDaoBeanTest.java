package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.api.DataAccessObjectTestCase;
import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;
import com.gl.training.dibrivnyi.planner.middle.entity.DailyPlan;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DailyPlanDaoBeanTest extends DataAccessObjectTestCase<DailyPlanDaoBean> {

    public DailyPlanDaoBeanTest() {
        super(DailyPlanDaoBean.class);
    }

    @Test
    public void testFindAllDailyPlansForUser_null_login() throws Exception {
        List<DailyPlan> result = bean.findAllDailyPlansForUser(null);

        assertThat(result).isEmpty();
    }

    @Test
    public void testFindAllDailyPlansForUser_user_has_not_plans() throws Exception {
        List<DailyPlan> result = bean.findAllDailyPlansForUser("tuser1");

        assertThat(result).isEmpty();
    }

    @Test
    public void testFindAllDailyPlansForUser() throws Exception {
        List<DailyPlan> result = bean.findAllDailyPlansForUser("mkasd");

        assertThat(result).hasSize(2).extracting("user").extracting("login").containsOnly("mkasd");
    }

    @Test
    public void testFindAllOpenedDailyPlansBeforeDate_no_such_plans() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2012);
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DATE, 1);

        List<DailyPlan> result = bean.findAllOpenedDailyPlansBeforeDate(calendar.getTime());

        assertThat(result).isEmpty();
    }

    @Test
    public void testFindAllOpenedDailyPlansBeforeDate_invalid_date() throws Exception {
        List<DailyPlan> result = bean.findAllOpenedDailyPlansBeforeDate(null);

        assertThat(result).isEmpty();
    }

    @Test
    public void testFindAllOpenedDailyPlansBeforeDate() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2014);
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
        calendar.set(Calendar.DATE, 1);

        List<DailyPlan> result = bean.findAllOpenedDailyPlansBeforeDate(calendar.getTime());

        assertThat(result).hasSize(1).extracting("state").containsOnly(DailyPlanState.OPENED);
    }

    @Test
    public void testFindOneByDateAndOwner_null_date() throws Exception {
        DailyPlan plan = bean.findOneByDateAndOwner(null, "mkasd");

        assertThat(plan).isNull();
    }

    @Test
    public void testFindOneByDateAndOwner_null_login() throws Exception {
        DailyPlan plan = bean.findOneByDateAndOwner(new Date(), null);

        assertThat(plan).isNull();
    }

    @Test
    public void testFindOneByDateAndOwner() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2013);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DATE, 12);
        Date date = DateUtils.truncate(calendar.getTime(), Calendar.DATE);

        DailyPlan dailyPlan = bean.findOneByDateAndOwner(date, "mkasd");

        assertThat(dailyPlan).isNotNull();
        assertThat(dailyPlan.getId()).isEqualTo(2);
        assertThat(dailyPlan.getDate().getTime()).isEqualTo(date.getTime());
        assertThat(dailyPlan.getState()).isEqualTo(DailyPlanState.CLOSED);
        assertThat(dailyPlan.getUser().getLogin()).isEqualTo("mkasd");
    }

    @Test
    public void testFindAllDailyPlans() throws Exception {
        List<DailyPlan> result = bean.findAllDailyPlans();

        assertThat(result).hasSize(4);
    }
}