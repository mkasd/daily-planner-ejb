package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.DailyPlanDao;
import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;
import com.gl.training.dibrivnyi.planner.middle.entity.DailyPlan;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alexandr Dibrivnyi
 *         14.08.14
 *         22:57
 */
public class ScheduleServiceImplTest {

    @Mock
    private DailyPlanDao dailyPlanDAO;
    @InjectMocks
    private ScheduleServiceImpl scheduleService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(dailyPlanDAO);
    }

    @Test
    public void testCloseDailyPlans_empty() throws Exception {
        when(dailyPlanDAO.findAllOpenedDailyPlansBeforeDate(any(Date.class))).thenReturn(new ArrayList<DailyPlan>());

        scheduleService.closeDailyPlans();

        verify(dailyPlanDAO).findAllOpenedDailyPlansBeforeDate(any(Date.class));
    }

    @Test
    public void testCloseDailyPlans() throws Exception {
        List<DailyPlan> dailyPlans = new ArrayList<>();
        dailyPlans.add(getDailyPlan());
        dailyPlans.add(getDailyPlan());

        when(dailyPlanDAO.findAllOpenedDailyPlansBeforeDate(any(Date.class))).thenReturn(dailyPlans);
        when(dailyPlanDAO.saveMultiple(any(List.class))).thenReturn(dailyPlans);

        scheduleService.closeDailyPlans();

        assertThat(dailyPlans).extracting("state").containsOnly(DailyPlanState.CLOSED);
        verify(dailyPlanDAO).findAllOpenedDailyPlansBeforeDate(any(Date.class));
        verify(dailyPlanDAO).saveMultiple(dailyPlans);
    }

    private DailyPlan getDailyPlan() {
        DailyPlan dailyPlan = new DailyPlan();
        dailyPlan.setState(DailyPlanState.OPENED);
        dailyPlan.setDate(new Date());
        return dailyPlan;
    }
}
