package com.gl.training.dibrivnyi.planner.middle.api;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * @author Alexandr Dibrivnyi
 *         22.08.14
 *         20:51
 */
public class DBUtil {

    private static final String PROPERTIES_FILE = "planner_test.properties";
    private static final String DATASETS_LOCATION = "datasets";
    private IDatabaseConnection databaseConnection;
    private Properties jdbcProperties;

    public DBUtil() {
        try {
            loadJdbcProperties();
            Connection jdbcConnection = DriverManager.getConnection(
                    jdbcProperties.getProperty("test.jdbc.url"),
                    jdbcProperties.getProperty("test.jdbc.user"),
                    jdbcProperties.getProperty("test.jdbc.password")
            );
            databaseConnection = new DatabaseConnection(jdbcConnection);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void loadJdbcProperties() throws IOException {
        jdbcProperties = new Properties();
        jdbcProperties.load(this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE));
    }

    public void setUpData() {
        try {
            FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
            FlatXmlDataSet dataSet = builder.build(
                    this.getClass().getClassLoader().getResourceAsStream(DATASETS_LOCATION + "/" + "dataset.xml")
            );
            DatabaseOperation.CLEAN_INSERT.execute(databaseConnection, dataSet);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void cleanUpData() {
        try {
            FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
            FlatXmlDataSet dataSet = builder.build(
                    this.getClass().getClassLoader().getResourceAsStream(DATASETS_LOCATION + "/" + "empty.xml")
            );
            DatabaseOperation.CLEAN_INSERT.execute(databaseConnection, dataSet);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
