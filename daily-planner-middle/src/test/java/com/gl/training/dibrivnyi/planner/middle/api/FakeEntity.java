package com.gl.training.dibrivnyi.planner.middle.api;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author oleksandr.dibrivniy
 *         29.08.14
 *         15:07
 */
@Entity
@Table(name = "fake_entity")
public class FakeEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "fake_first_field")
    private String fakeFirstField;

    @Column(name = "fake_second_field")
    private String fakeSecondField;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFakeFirstField() {
        return fakeFirstField;
    }

    public void setFakeFirstField(String fakeFirstField) {
        this.fakeFirstField = fakeFirstField;
    }

    public String getFakeSecondField() {
        return fakeSecondField;
    }

    public void setFakeSecondField(String fakeSecondField) {
        this.fakeSecondField = fakeSecondField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FakeEntity that = (FakeEntity) o;

        if (fakeFirstField != null ? !fakeFirstField.equals(that.fakeFirstField) : that.fakeFirstField != null)
            return false;
        if (fakeSecondField != null ? !fakeSecondField.equals(that.fakeSecondField) : that.fakeSecondField != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (fakeFirstField != null ? fakeFirstField.hashCode() : 0);
        result = 31 * result + (fakeSecondField != null ? fakeSecondField.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FakeEntity{" +
                "id=" + id +
                ", fakeFirstField='" + fakeFirstField + '\'' +
                ", fakeSecondField='" + fakeSecondField + '\'' +
                '}';
    }
}
