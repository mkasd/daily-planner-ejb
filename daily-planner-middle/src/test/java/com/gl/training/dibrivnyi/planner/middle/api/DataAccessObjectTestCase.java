package com.gl.training.dibrivnyi.planner.middle.api;

import com.gl.training.dibrivnyi.planner.middle.dao.AbstractDao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Alexandr Dibrivnyi
 *         22.08.14
 *         20:48
 */
public class DataAccessObjectTestCase<T extends AbstractDao> {

    private static final String PERSISTENCE_UNIT_NAME = "dailyPlannerPUTest";
    protected T bean;
    protected EntityManager em;
    private static EntityManagerFactory emf;
    private DBUtil dbUtil;

    public DataAccessObjectTestCase(Class<T> implementation) {
        try {
            em = emf.createEntityManager();
            bean = implementation.newInstance();
            bean.setEntityManager(em);
            dbUtil = new DBUtil();
        } catch (Exception e) {
            throw new RuntimeException("Error while creating new instance of tested bean");
        }
    }

    @BeforeClass
    public static void setUpClass() {
        emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    @AfterClass
    public static void tearDownClass() {
        emf.close();
    }

    @Before
    public void setUp() {
        em = emf.createEntityManager();
        dbUtil.setUpData();
    }

    @After
    public void tearDown() {
        if (em.getTransaction().isActive()) {
            em.getTransaction().rollback();
        }
        if (em.isOpen()) {
            em.close();
        }
        dbUtil.cleanUpData();
    }

    protected void executeWithTransaction(Action action) {
        em.getTransaction().begin();
        action.execute();
        em.getTransaction().commit();
    }

    protected interface Action {
        void execute();
    }
}
