package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.api.DataAccessObjectTestCase;
import com.gl.training.dibrivnyi.planner.middle.entity.User;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Alexandr Dibrivnyi
 *         23.08.14
 *         16:16
 */
public class UserDaoBeanTest extends DataAccessObjectTestCase<UserDaoBean> {

    public UserDaoBeanTest() {
        super(UserDaoBean.class);
    }

    @Test
    public void testFindOneByLogin() throws Exception {
        User user = bean.findOneByLogin("mkasd");

        assertThat(user.getLogin()).isEqualTo("mkasd");
        assertThat(user.getEmail()).isEqualTo("test@mkasd.com");
        assertThat(user.getPassword()).isEqualTo("914656b0ef7f95058b88ba99d889949b207c447b");
        assertThat(user.getFirstName()).isEqualTo("mkasd");
        assertThat(user.getLastName()).isEqualTo("mkasd");
        assertThat(user.getDateOfBirth().toString()).isEqualTo("1991-03-04");
    }

    @Test
    public void testFindOneByLogin_no_such_user() throws Exception {
        User user = bean.findOneByLogin("no_such_user");

        assertThat(user).isNull();
    }

    @Test
    public void testFindOneByLogin_null_login() throws Exception {
        User user = bean.findOneByLogin(null);

        assertThat(user).isNull();
    }

    @Test
    public void testFindAllLogins() throws Exception {
        List<String> logins = bean.findAllLogins();

        assertThat(logins).hasSize(3).containsOnly("mkasd", "tuser", "tuser1");
    }

    @Test
    public void testFindAllEmails() throws Exception {
        List<String> emails = bean.findAllEmails();

        assertThat(emails).hasSize(3).containsOnly("test@mkasd.com", "test@tuser.com", "test@tuser1.com");
    }

    @Test
    public void testFindAll() throws Exception {
        List<User> users = bean.findAllUsers();

        assertThat(users).hasSize(3);
        assertThat(users).extracting("login").containsOnly("mkasd", "tuser", "tuser1");
        assertThat(users).extracting("email").containsOnly("test@mkasd.com", "test@tuser.com", "test@tuser1.com");
    }
}
