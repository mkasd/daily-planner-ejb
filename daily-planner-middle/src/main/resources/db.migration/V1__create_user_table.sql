CREATE TABLE users
(
  date_of_birth date NOT NULL,
  email character varying(255) NOT NULL,
  first_name character varying(255) NOT NULL,
  hobbies character varying(255),
  last_name character varying(255) NOT NULL,
  login character varying(64) NOT NULL,
  notes character varying(255),
  password character varying(255) NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (login),
  CONSTRAINT uk_constraint_unique_email UNIQUE (email)
);