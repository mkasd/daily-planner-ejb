CREATE TABLE daily_plan
(
  id    BIGINT                 NOT NULL,
  date  DATE                   NOT NULL,
  state CHARACTER VARYING(255) NOT NULL,
  owner CHARACTER VARYING(64)  NOT NULL,
  CONSTRAINT daily_plan_pkey PRIMARY KEY (id),
  CONSTRAINT fk_daily_plan FOREIGN KEY (owner)
  REFERENCES users (login) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE task
(
  id          BIGINT                   NOT NULL,
  description CHARACTER VARYING(40096) NOT NULL,
  end_time    TIME WITHOUT TIME ZONE   NOT NULL,
  start_time  TIME WITHOUT TIME ZONE   NOT NULL,
  title       CHARACTER VARYING(255)   NOT NULL,
  task        BIGINT,
  CONSTRAINT task_pkey PRIMARY KEY (id),
  CONSTRAINT fk_task FOREIGN KEY (task)
  REFERENCES daily_plan (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);