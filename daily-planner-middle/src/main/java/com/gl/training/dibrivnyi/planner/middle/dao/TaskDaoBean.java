package com.gl.training.dibrivnyi.planner.middle.dao;

import javax.ejb.Stateless;

import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.TaskDao;
import com.gl.training.dibrivnyi.planner.middle.entity.Task;

/**
 * @author Alexandr Dibrivnyi
 *         22.08.14
 *         18:59
 */
@Stateless
public class TaskDaoBean extends AbstractDao<Task, Long> implements TaskDao {
}
