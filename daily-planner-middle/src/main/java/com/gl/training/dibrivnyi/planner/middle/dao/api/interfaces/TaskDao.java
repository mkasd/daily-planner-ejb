package com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces;


import com.gl.training.dibrivnyi.planner.middle.entity.Task;

/**
 * @author Alexandr Dibrivnyi
 *         06.08.14
 *         23:00
 */
public interface TaskDao extends CommonDao<Task, Long> {
}