package com.gl.training.dibrivnyi.planner.middle.dto;

/**
 * @author oleksandr.dibrivniy
 *         27.08.14
 *         15:27
 */
public abstract class AppResources {

    public static final String JMS_CONNECTION_FACTORY_NAME = "dp/jms/DailyPlannerConnectionFactory";
    public static final String EMAIL_QUEUE_NAME = "dp/jms/emailQueue";
}
