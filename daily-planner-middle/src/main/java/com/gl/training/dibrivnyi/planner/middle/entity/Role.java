package com.gl.training.dibrivnyi.planner.middle.entity;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         17:14
 */
public enum Role {
    USER,
    ADMIN
}
