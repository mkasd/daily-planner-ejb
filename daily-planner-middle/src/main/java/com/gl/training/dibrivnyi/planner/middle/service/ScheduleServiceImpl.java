package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.DailyPlanDao;
import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;
import com.gl.training.dibrivnyi.planner.middle.entity.DailyPlan;
import com.gl.training.dibrivnyi.planner.middle.service.api.ScheduleService;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Alexandr Dibrivnyi
 *         14.08.14
 *         22:36
 */
@Singleton
public class ScheduleServiceImpl implements ScheduleService {

    private static final Logger LOGGER = Logger.getLogger(ScheduleServiceImpl.class.getSimpleName());
    private DailyPlanDao dailyPlanDAO;

    @Inject
    public void setDailyPlanDAO(DailyPlanDao dailyPlanDAO) {
        this.dailyPlanDAO = dailyPlanDAO;
    }

    /**
     * Method for scheduling closing task. Runs every date at 0.10 AM
     */
//    @Scheduled(cron = "0 10 0 * * ?")
    public void closeDailyPlans() {
        Date currentDate = new Date();
        List<DailyPlan> openedDailyPlans = dailyPlanDAO.findAllOpenedDailyPlansBeforeDate(currentDate);
        if (!openedDailyPlans.isEmpty()) {
            for (DailyPlan openedDailyPlan : openedDailyPlans) {
                openedDailyPlan.setState(DailyPlanState.CLOSED);
            }
            dailyPlanDAO.saveMultiple(openedDailyPlans);
            LOGGER.info(String.format("Closed <%s> daily plans", openedDailyPlans.size()));
        } else LOGGER.info("There are no opened daily plans");
    }
}
