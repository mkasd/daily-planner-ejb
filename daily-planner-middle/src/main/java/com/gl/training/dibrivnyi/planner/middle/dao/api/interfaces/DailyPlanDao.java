package com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces;


import com.gl.training.dibrivnyi.planner.middle.entity.DailyPlan;

import java.util.Date;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         06.08.14
 *         23:01
 */
public interface DailyPlanDao extends CommonDao<DailyPlan, Long> {

    /**
     * Method for loading all daily plan for selected user
     *
     * @param login - user login
     * @return - list of daily plans for selected users
     */
    public List<DailyPlan> findAllDailyPlansForUser(String login);

    /**
     * Method for loading all opened daily plans before selected date
     *
     * @param currentDate - selected date
     * @return - list of all opened daily plans before selected date
     */
    public List<DailyPlan> findAllOpenedDailyPlansBeforeDate(Date currentDate);

    /**
     * Method for loading daily plan by owner and plan date
     *
     * @param planDate   - selected date
     * @param ownerLogin - plan owner
     * @return - plan details
     */
    public DailyPlan findOneByDateAndOwner(Date planDate, String ownerLogin);

    /**
     * Method for loading all daily plans
     *
     * @return - list of all daily plans
     */
    public List<DailyPlan> findAllDailyPlans();
}
