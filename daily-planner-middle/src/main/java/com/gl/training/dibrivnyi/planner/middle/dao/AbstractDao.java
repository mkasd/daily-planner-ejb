package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         22.08.14
 *         0:25
 */
@SuppressWarnings("unchecked")
public abstract class AbstractDao<T, PK extends Serializable> implements CommonDao<T, PK> {

    @PersistenceContext
    private EntityManager entityManager;
    private Class<T> entityClass;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public AbstractDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao for details
     */
    @Override
    public T findOne(PK id) {
        return entityManager.find(entityClass, id);
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao for details
     */
    @Override
    public T save(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao for details
     */
    @Override
    public void deleteById(PK id) {
        T entity = findOne(id);
        if (entity != null) {
            entityManager.remove(entity);
        }
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao for details
     */
    @Override
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao for details
     */
    @Override
    public void deleteMultiple(Iterable<T> entities) {
        for (T entity : entities) {
            delete(entity);
        }
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao for details
     */
    @Override
    public List<T> saveMultiple(List<T> entities) {
        for (T entity : entities) {
            save(entity);
        }
        return entities;
    }
}
