package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dto.AppResources;
import com.gl.training.dibrivnyi.planner.middle.dto.EmailMessage;
import com.gl.training.dibrivnyi.planner.middle.service.api.MailService;
import com.gl.training.dibrivnyi.planner.middle.util.JsonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * @author oleksandr.dibrivniy
 *         25.08.14
 *         13:52
 */
@MessageDriven(
        mappedName = AppResources.EMAIL_QUEUE_NAME,
        activationConfig = {
        		@ActivationConfigProperty(propertyName = "connectionFactoryLookup", propertyValue = AppResources.JMS_CONNECTION_FACTORY_NAME),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        }
)
public class MailMessageListener implements MessageListener {

    private static final Logger log = LogManager.getLogger(MailMessageListener.class.getSimpleName());
    private MailService mailService;

    @Inject
    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    @Override
    public void onMessage(Message message) {
        log.info("Trying to send email message");
        try {
            if (message instanceof TextMessage) {
                String json = ((TextMessage) message).getText();
                EmailMessage emailMessage = JsonUtil.fromJson(json, EmailMessage.class);
                mailService.sendEmail(emailMessage);
                log.info("Email message was sent successfully");
            }
        } catch (JMSException e) {
            log.warn("Failed while sending email message", e);
        }
    }
}
