package com.gl.training.dibrivnyi.planner.middle.util;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Alexandr Dibrivnyi
 *         16.08.14
 *         22:24
 */
public class DateUtil {

    public static boolean isDayBefore(Date selectedDate) {
        Date currentDate = new Date();
        currentDate = DateUtils.truncate(currentDate, Calendar.DATE);
        selectedDate = DateUtils.truncate(selectedDate, Calendar.DATE);
        return selectedDate.getTime() < currentDate.getTime();
    }
}
