package com.gl.training.dibrivnyi.planner.middle.exception;

/**
 * @author Alexandr Dibrivnyi
 *         16.08.14
 *         22:00
 *
 *         Should be thrown while trying to persist duplicated daily plan entity
 */
public class DuplicatedPlanException extends Exception {
    public DuplicatedPlanException() {
    }

    public DuplicatedPlanException(String message) {
        super(message);
    }
}
