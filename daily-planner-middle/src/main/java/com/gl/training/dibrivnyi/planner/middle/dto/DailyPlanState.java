package com.gl.training.dibrivnyi.planner.middle.dto;

/**
 * @author Alexandr Dibrivnyi
 *         14.08.14
 *         22:40
 */
public enum DailyPlanState {
    OPENED, CLOSED
}
