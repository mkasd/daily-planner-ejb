package com.gl.training.dibrivnyi.planner.middle.service;


import com.gl.training.dibrivnyi.planner.middle.dto.EmailMessage;
import com.gl.training.dibrivnyi.planner.middle.service.api.MailService;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         11:44
 */
@Singleton
public class MailServiceImpl implements MailService {

    private static final Logger log = LogManager.getLogger(MailServiceImpl.class.getSimpleName());
    @Resource(lookup = "mail/PrimarySession")
    private Session mailSession;

    @Override
    public void sendEmail(EmailMessage emailMessage) {
        try {
            MimeMessage message = new MimeMessage(mailSession);
            message.setSubject(emailMessage.getSubject());
            message.setText(emailMessage.getDescription());
            message.setRecipients(Message.RecipientType.TO, emailMessage.getTo());

            Transport.send(message);
            log.info(String.format("Message with subject <%s> was sent to <%s>", emailMessage.getSubject(), emailMessage.getTo()));
        } catch (MessagingException e) {
            log.info("Error while sending email message", e);
        }
    }
}
