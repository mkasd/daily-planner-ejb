package com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces;

import java.io.Serializable;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         22.08.14
 *         0:20
 */
public interface CommonDao<T, PK extends Serializable> {

    /**
     * Template method for find entity
     *
     * @param id - entity id
     * @return - found entity or null if it does not exist
     */
    T findOne(PK id);

    /**
     * Template method for saving / updating entity
     * If entity is new, then it will be saved as new object, otherwise old entity will be updated
     *
     * @param entity - entity to save / update
     * @return - saved / updated entity
     */
    T save(T entity);

    /**
     * Template method for saving / updating entities
     * If each entity from list is new, then it will be saved as new object, otherwise old entity will be updated
     * @param entities - entities to save / update
     */
    List<T> saveMultiple(List<T> entities);

    /**
     * Template method for deleting entity by its id
     * If entity not found, there are no any actions
     *
     * @param id - entity id
     */
    void deleteById(PK id);

    /**
     * Template method for deleting entity
     *
     * @param entity - entity to delete
     */
    void delete(T entity);

    /**
     * Template method for deleting list of managed entities
     * @param entities - managed entities will be removed
     */
    void deleteMultiple(Iterable<T> entities);
}
