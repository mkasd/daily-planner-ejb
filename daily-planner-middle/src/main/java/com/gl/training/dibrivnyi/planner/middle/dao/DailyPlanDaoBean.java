package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.DailyPlanDao;
import com.gl.training.dibrivnyi.planner.middle.entity.DailyPlan;

import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         22.08.14
 *         18:54
 */
@Stateless
public class DailyPlanDaoBean extends AbstractDao<DailyPlan, Long> implements DailyPlanDao {

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao
     */
    @Override
    public List<DailyPlan> findAllDailyPlansForUser(String login) {
        return getEntityManager().createQuery("select dp from DailyPlan dp where dp.user.login =:login order by dp.date asc", DailyPlan.class)
                .setParameter("login", login)
                .getResultList();
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao
     */
    @Override
    public List<DailyPlan> findAllOpenedDailyPlansBeforeDate(Date currentDate) {
        return getEntityManager().createQuery("select dp from DailyPlan dp where dp.state = 'OPENED' and dp.date <= :currentDate", DailyPlan.class)
                .setParameter("currentDate", currentDate)
                .getResultList();
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao
     */
    @Override
    public DailyPlan findOneByDateAndOwner(Date planDate, String ownerLogin) {
        List<DailyPlan> dailyPlans = getEntityManager().createQuery("select dp from DailyPlan dp where dp.date =:date and dp.user.login =:login", DailyPlan.class)
                .setParameter("date", planDate)
                .setParameter("login", ownerLogin)
                .getResultList();
        if (dailyPlans.isEmpty()) {
            return null;
        }
        return dailyPlans.get(0);
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.CommonDao
     */
    @Override
    public List<DailyPlan> findAllDailyPlans() {
        return getEntityManager().createQuery("select dp from DailyPlan dp", DailyPlan.class).getResultList();
    }
}
