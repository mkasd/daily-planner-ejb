package com.gl.training.dibrivnyi.planner.middle.util;

import com.google.gson.Gson;

/**
 * @author oleksandr.dibrivniy
 *         25.08.14
 *         14:06
 */
public class JsonUtil {
    private static Gson gson = new Gson();

    public static String toJson(Object obj) {
        return gson.toJson(obj);
    }

    public static <T> T fromJson(String json, Class<T> typeClass) {
        return gson.fromJson(json, typeClass);
    }
}
