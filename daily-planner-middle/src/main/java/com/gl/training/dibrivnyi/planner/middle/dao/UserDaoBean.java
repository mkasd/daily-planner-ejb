package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.UserDao;
import com.gl.training.dibrivnyi.planner.middle.entity.User;

import javax.ejb.Stateless;
import java.util.List;

/**
 * @author oleksandr.dibrivniy
 *         22.08.14
 *         14:17
 */
@Stateless
public class UserDaoBean extends AbstractDao<User, Long> implements UserDao {

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.UserDao for details
     */
    @Override
    public User findOneByLogin(String login) {
        List<User> users = getEntityManager().createQuery("select u from User u where u.login =:login", User.class)
                .setParameter("login", login)
                .getResultList();
        if (users.isEmpty()) {
            return null;
        } else return users.get(0);
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.UserDao for details
     */
    @Override
    public List<String> findAllLogins() {
        return getEntityManager().createQuery("select u.login from User u", String.class).getResultList();
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.UserDao for details
     */
    @Override
    public List<String> findAllEmails() {
        return getEntityManager().createQuery("select u.email from User u", String.class).getResultList();
    }

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.UserDao for details
     */
    @Override
    public List<User> findAllUsers() {
        return getEntityManager().createQuery("select u from User u", User.class).getResultList();
    }
}
