package com.gl.training.dibrivnyi.planner.middle.properties.builder;

import com.gl.training.dibrivnyi.planner.middle.properties.SecurityProperties;

/**
 * @author Alexandr Dibrivnyi
 *         18.08.14
 *         19:53
 */
public class SecurityPropertiesBuilder {
    private String defaultAdminLogin;
    private String defaultAdminPassword;

    private SecurityPropertiesBuilder() {
    }

    public static SecurityPropertiesBuilder aSecurityProperties() {
        return new SecurityPropertiesBuilder();
    }

    public SecurityPropertiesBuilder withDefaultAdminLogin(String defaultAdminLogin) {
        this.defaultAdminLogin = defaultAdminLogin;
        return this;
    }

    public SecurityPropertiesBuilder withDefaultAdminPassword(String defaultAdminPassword) {
        this.defaultAdminPassword = defaultAdminPassword;
        return this;
    }

    public SecurityProperties build() {
        SecurityProperties securityProperties = new SecurityProperties();
        securityProperties.setDefaultAdminLogin(defaultAdminLogin);
        securityProperties.setDefaultAdminPassword(defaultAdminPassword);
        return securityProperties;
    }
}
