package com.gl.training.dibrivnyi.planner.middle.entity;

import com.gl.training.dibrivnyi.planner.middle.dto.DailyPlanState;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         06.08.14
 *         22:38
 */
@Entity
@Table(name = "daily_plan")
public class DailyPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "owner", nullable = false)
    private User user;

    @OneToMany
    @JoinColumn(name = "task")
    private List<Task> dayTasks;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DailyPlanState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Task> getDayTasks() {
        return dayTasks;
    }

    public void setDayTasks(List<Task> dayTasks) {
        this.dayTasks = dayTasks;
    }

    public DailyPlanState getState() {
        return state;
    }

    public void setState(DailyPlanState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DailyPlan dailyPlan = (DailyPlan) o;

        if (date != null ? !date.equals(dailyPlan.date) : dailyPlan.date != null) return false;
        if (dayTasks != null ? !dayTasks.equals(dailyPlan.dayTasks) : dailyPlan.dayTasks != null) return false;
        if (id != null ? !id.equals(dailyPlan.id) : dailyPlan.id != null) return false;
        if (state != dailyPlan.state) return false;
        if (user != null ? !user.equals(dailyPlan.user) : dailyPlan.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (dayTasks != null ? dayTasks.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DailyPlan{" +
                "id=" + id +
                ", date=" + date +
                ", user=" + user +
                ", state=" + state +
                '}';
    }
}
