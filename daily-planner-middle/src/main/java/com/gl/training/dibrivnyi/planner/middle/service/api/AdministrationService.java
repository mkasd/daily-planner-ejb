package com.gl.training.dibrivnyi.planner.middle.service.api;


import com.gl.training.dibrivnyi.planner.middle.exception.UserNotFoundException;
import com.gl.training.dibrivnyi.planner.middle.entity.Permission;
import com.gl.training.dibrivnyi.planner.middle.entity.User;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:25
 */
public interface AdministrationService {

    void addUser(User user);

    void removeUser(Long id);

    List<User> listAllUsers();

    User getUser(String login) throws UserNotFoundException;

    List<Permission> findUserPermissionByLogin(String login);

    void updateUser(User user);

    List<String> findAllLogins();

    List<String> findAllEmails();
}
