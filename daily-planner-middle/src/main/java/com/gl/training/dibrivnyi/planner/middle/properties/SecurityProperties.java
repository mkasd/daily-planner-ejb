package com.gl.training.dibrivnyi.planner.middle.properties;

/**
 * @author Alexandr Dibrivnyi
 *         18.08.14
 *         19:52
 */
public class SecurityProperties {

    private String defaultAdminLogin;
    private String defaultAdminPassword;

    public String getDefaultAdminLogin() {
        return defaultAdminLogin;
    }

    public void setDefaultAdminLogin(String defaultAdminLogin) {
        this.defaultAdminLogin = defaultAdminLogin;
    }

    public String getDefaultAdminPassword() {
        return defaultAdminPassword;
    }

    public void setDefaultAdminPassword(String defaultAdminPassword) {
        this.defaultAdminPassword = defaultAdminPassword;
    }
}
