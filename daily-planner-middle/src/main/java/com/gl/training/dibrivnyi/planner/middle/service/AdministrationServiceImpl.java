package com.gl.training.dibrivnyi.planner.middle.service;


import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.PermissionDao;
import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.UserDao;
import com.gl.training.dibrivnyi.planner.middle.entity.Permission;
import com.gl.training.dibrivnyi.planner.middle.entity.Role;
import com.gl.training.dibrivnyi.planner.middle.entity.User;
import com.gl.training.dibrivnyi.planner.middle.exception.UserNotFoundException;
import com.gl.training.dibrivnyi.planner.middle.service.api.AdministrationService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:27
 */
@Stateless
public class AdministrationServiceImpl implements AdministrationService {

    private static final Logger log = LogManager.getLogger(AdministrationServiceImpl.class.getSimpleName());
    private UserDao userDAO;
    private PermissionDao permissionDAO;

    @Inject
    public void setUserDAO(UserDao userDAO) {
        this.userDAO = userDAO;
    }

    @Inject
    public void setPermissionDAO(PermissionDao permissionDAO) {
        this.permissionDAO = permissionDAO;
    }

    /**
     * Method for persisting users. Encodes input user password to sha1Hex;
     * Sets user default role as 'USER'
     *
     * @param user - user to persist
     */
    @Override
    public void addUser(User user) {
        String encodedPassword = DigestUtils.sha1Hex(user.getPassword());
        user.setPassword(encodedPassword);

        Permission userPermission = new Permission();
        userPermission.setOwner(user);
        userPermission.setRole(Role.USER);

        userDAO.save(user);
        permissionDAO.save(userPermission);
        log.info(String.format("User <%s> was saved in database", user));
    }

    /**
     * Method for removing user by his unique id
     * If user not found it do nothing
     *
     * @param id - user id that will be deleted
     */
    @Override
    public void removeUser(Long id) {
        User user = userDAO.findOne(id);
        if (user != null) {
            List<Permission> userPermissions = permissionDAO.findUserPermissions(user.getLogin());
            permissionDAO.deleteMultiple(userPermissions);
            userDAO.delete(user);
            log.info(String.format("User %s was removed", user));
        }
    }

    /**
     * Method for loading list of registered users
     *
     * @return list of all users
     */
    @Override
    public List<User> listAllUsers() {
        log.info("Start loading list of all users");
        List<User> users = userDAO.findAllUsers();
        log.info("Totally loaded users: " + users.size());
        return users;
    }

    /**
     * Method for loading users details
     *
     * @param login - user login, which details will be loaded
     * @return user details
     * @throws UserNotFoundException - if user not found
     */
    @Override
    public User getUser(String login) throws UserNotFoundException {
        User user = userDAO.findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException(login);
        }
        log.info(String.format("User %s was found in database", user));
        return user;
    }

    /**
     * Method for loading user permission by user login
     *
     * @param login - user login, which permission should be found
     * @return - list of user permission
     */
    @Override
    public List<Permission> findUserPermissionByLogin(String login) {
        List<Permission> permissions = permissionDAO.findUserPermissions(login);
        log.info(String.format("User %s has permissions %s", login, permissions.toString()));
        return permissions;
    }

    /**
     * Method for updating user. If user is new, it will be saved as a new user
     *
     * @param user - user details to update / save
     */
    @Override
    public void updateUser(User user) {
        userDAO.save(user);
        log.info(String.format("User %s was updated", user));
    }

    /**
     * Method for loading all logins
     *
     * @return - list of all logins
     */
    @Override
    public List<String> findAllLogins() {
        return userDAO.findAllLogins();
    }

    /**
     * Method for loading all email
     *
     * @return - list of all email
     */
    @Override
    public List<String> findAllEmails() {
        return userDAO.findAllEmails();
    }
}
