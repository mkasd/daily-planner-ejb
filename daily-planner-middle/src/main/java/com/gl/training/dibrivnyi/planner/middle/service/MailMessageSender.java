package com.gl.training.dibrivnyi.planner.middle.service;

import com.gl.training.dibrivnyi.planner.middle.dto.AppResources;
import com.gl.training.dibrivnyi.planner.middle.dto.EmailMessage;
import com.gl.training.dibrivnyi.planner.middle.util.JsonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.*;

/**
 * @author oleksandr.dibrivniy
 *         25.08.14
 *         15:40
 */
@Stateless
public class MailMessageSender {

    private static final Logger log = LogManager.getLogger(MailMessageSender.class.getSimpleName());
    @Resource(lookup = AppResources.JMS_CONNECTION_FACTORY_NAME)
    private ConnectionFactory connectionFactory;
    @Resource(lookup = AppResources.EMAIL_QUEUE_NAME)
    private Queue messageQueue;

    public void sendMessage(EmailMessage emailMessage) {
        try {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(messageQueue);
            TextMessage message = session.createTextMessage(JsonUtil.toJson(emailMessage));
            messageProducer.send(message);
            session.close();
            connection.close();
        } catch (JMSException e) {
            log.error("Error while sending message to jms queue", e);
        }
    }
}
