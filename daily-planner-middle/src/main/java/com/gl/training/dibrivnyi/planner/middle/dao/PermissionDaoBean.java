package com.gl.training.dibrivnyi.planner.middle.dao;

import com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.PermissionDao;
import com.gl.training.dibrivnyi.planner.middle.entity.Permission;

import javax.ejb.Stateless;
import java.util.List;

/**
 * @author oleksandr.dibrivniy
 *         22.08.14
 *         14:28
 */
@Stateless
public class PermissionDaoBean extends AbstractDao<Permission, Long> implements PermissionDao {

    /**
     * @see com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces.PermissionDao for details
     */
    @Override
    public List<Permission> findUserPermissions(String login) {
        return getEntityManager().createQuery("select p from Permission p where p.owner.login =:login", Permission.class)
                .setParameter("login", login)
                .getResultList();
    }
}
