package com.gl.training.dibrivnyi.planner.middle;

import com.gl.training.dibrivnyi.planner.middle.properties.SecurityProperties;
import com.gl.training.dibrivnyi.planner.middle.properties.builder.SecurityPropertiesBuilder;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         20:36
 */
public class ExternalConfiguration {

    private static final ExternalConfiguration INSTANCE = new ExternalConfiguration();
    private static final String CONFIG_NAME = "planner.properties";
    private Properties externalConfig;

    private ExternalConfiguration() {
        externalConfig = new Properties();
        try {
            externalConfig.load(this.getClass().getClassLoader().getResourceAsStream(CONFIG_NAME));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ExternalConfiguration getInstance() {
        return INSTANCE;
    }
}
