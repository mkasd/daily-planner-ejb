package com.gl.training.dibrivnyi.planner.middle.service.api;

import com.gl.training.dibrivnyi.planner.middle.dto.EmailMessage;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         11:42
 */
public interface MailService {

    void sendEmail(EmailMessage emailMessage);
}
