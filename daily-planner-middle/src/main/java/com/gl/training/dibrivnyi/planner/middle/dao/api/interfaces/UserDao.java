package com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces;


import com.gl.training.dibrivnyi.planner.middle.entity.User;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         04.08.14
 *         21:22
 */
public interface UserDao extends CommonDao<User, Long> {

    /**
     * Method for loading user details by login
     *
     * @param login - user login
     * @return - loaded user details
     */
    @Deprecated
    User findOneByLogin(String login);

    /**
     * Method for loading all logins
     *
     * @return - list of logins
     */
//    @Query("select u.login from User u")
    List<String> findAllLogins();

    /**
     * Method for loading all emails
     *
     * @return - list of emails
     */
    List<String> findAllEmails();

    /**
     * Method for loading list of all users details
     *
     * @return - all users details
     */
    List<User> findAllUsers();
}
