package com.gl.training.dibrivnyi.planner.middle.dao.api.interfaces;


import com.gl.training.dibrivnyi.planner.middle.entity.Permission;

import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         12.08.14
 *         23:29
 */
public interface PermissionDao extends CommonDao<Permission, Long> {

    /**
     * Method for loading user permissions
     *
     * @param login - user login
     * @return - list of available user permissions
     */
    List<Permission> findUserPermissions(String login);
}
