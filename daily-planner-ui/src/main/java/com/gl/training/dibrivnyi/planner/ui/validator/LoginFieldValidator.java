package com.gl.training.dibrivnyi.planner.ui.validator;

import com.gl.training.dibrivnyi.planner.middle.service.api.AdministrationService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         12.08.14
 *         22:45
 */
@Named
@ApplicationScoped
public class LoginFieldValidator implements Validator {

    private static final int MIN_LOGIN_LENGTH = 5;

    @Inject
    private AdministrationService administrationService;

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        String login = o.toString();
        if (login.length() < MIN_LOGIN_LENGTH) {
            throwValidationException("Login too short. Min length - 5 symbols");
        }
        List<String> allLogins = administrationService.findAllLogins();
        if (allLogins.contains(login)) {
            throwValidationException("Login is already in use");
        }
    }

    private void throwValidationException(String summary) {
        FacesMessage msg = new FacesMessage(summary);
        msg.setSeverity(FacesMessage.SEVERITY_WARN);
        throw new ValidatorException(msg);
    }
}
