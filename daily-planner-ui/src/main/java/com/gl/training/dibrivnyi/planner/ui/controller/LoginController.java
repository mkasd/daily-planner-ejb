package com.gl.training.dibrivnyi.planner.ui.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * @author Alexandr Dibrivnyi
 *         15.08.14
 *         22:12
 */
@Named
@RequestScoped
public class LoginController {

    private String login;
    private String password;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean performLogin() {
        return true; //todo
    }
}
