package com.gl.training.dibrivnyi.planner.ui.controller;

import com.gl.training.dibrivnyi.planner.middle.dto.EmailMessage;
import com.gl.training.dibrivnyi.planner.middle.service.MailMessageSender;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         13.08.14
 *         12:23
 */
@Named
@ApplicationScoped
public class SendMailController {

    @Inject
    private MailMessageSender mailMessageSender;
    private List<String> recipients;
    private String subject;
    private String description;

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void sendMessage() {
        for (String recipient : recipients) {
            EmailMessage emailMessage = new EmailMessage();
            emailMessage.setDescription(description);
            emailMessage.setSubject(subject);
            emailMessage.setTo(recipient);
            mailMessageSender.sendMessage(emailMessage);
        }
        FacesMessage msg = new FacesMessage();
        msg.setSummary(String.format("Message with subject <%s> was sent to <%s> recipients", subject, recipients.size()));
        msg.setSeverity(FacesMessage.SEVERITY_INFO);

        FacesContext.getCurrentInstance().addMessage(null, msg);
        recipients = null;
        subject = null;
        description = null;
    }
}
