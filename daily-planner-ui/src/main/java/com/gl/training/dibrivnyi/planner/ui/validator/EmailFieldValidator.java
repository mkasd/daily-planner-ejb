package com.gl.training.dibrivnyi.planner.ui.validator;

import com.gl.training.dibrivnyi.planner.middle.service.api.AdministrationService;
import org.apache.commons.validator.routines.EmailValidator;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         12.08.14
 *         22:45
 */
@Named
@ApplicationScoped
public class EmailFieldValidator implements Validator {

    @Inject
    private AdministrationService administrationService;

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {
        EmailValidator emailValidator = EmailValidator.getInstance();
        String email = o.toString();
        boolean valid = emailValidator.isValid(email);
        if (!valid) {
            throwValidationException("Email is not valid");
        }
        List<String> allEmails = administrationService.findAllEmails();
        if (allEmails.contains(email)) {
            throwValidationException("Email is already in use");
        }
    }

    private void throwValidationException(String summary) {
        FacesMessage msg = new FacesMessage();
        msg.setSummary(summary);
        msg.setSeverity(FacesMessage.SEVERITY_WARN);
        throw new ValidatorException(msg);
    }
}
