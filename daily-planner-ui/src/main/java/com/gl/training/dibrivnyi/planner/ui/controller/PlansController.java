package com.gl.training.dibrivnyi.planner.ui.controller;

import com.gl.training.dibrivnyi.planner.middle.entity.DailyPlan;
import com.gl.training.dibrivnyi.planner.middle.entity.Task;
import com.gl.training.dibrivnyi.planner.middle.exception.DuplicatedPlanException;
import com.gl.training.dibrivnyi.planner.middle.service.api.TaskService;
import com.gl.training.dibrivnyi.planner.middle.util.DateUtil;
import com.gl.training.dibrivnyi.planner.ui.api.Actions;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * @author Alexandr Dibrivnyi
 *         16.08.14
 *         21:50
 */
@Named
@SessionScoped
public class PlansController {

    @Inject
    private TaskService taskService;
    private Date dailyPlanDate;
    private DailyPlan selectedDailyPlan;
    private Task newTask;
    private List<Task> selectedTasks;

    @PostConstruct
    public void initController() {
        newTask = new Task();
        selectedDailyPlan = new DailyPlan();
    }

    public List<Task> getSelectedTasks() {
        return selectedTasks;
    }

    public void setSelectedTasks(List<Task> selectedTasks) {
        this.selectedTasks = selectedTasks;
    }

    public void removeSelectedTasks() {
        taskService.deleteTasks(selectedTasks);

        showMessage(String.format("Deleted <%s> tasks", selectedTasks.size()), FacesMessage.SEVERITY_INFO);
    }

    public Task firstSelectedTask() {
        return selectedTasks.get(0);
    }

    public Task getNewTask() {
        return newTask;
    }

    public void setNewTask(Task newTask) {
        this.newTask = newTask;
    }

    public DailyPlan getSelectedDailyPlan() {
        return selectedDailyPlan;
    }

    public void setSelectedDailyPlan(DailyPlan selectedDailyPlan) {
        this.selectedDailyPlan = selectedDailyPlan;
    }

    public Date getDailyPlanDate() {
        return dailyPlanDate;
    }

    public void setDailyPlanDate(Date dailyPlanDate) {
        this.dailyPlanDate = dailyPlanDate;
    }

    public List<DailyPlan> listAllDailyPlansForCurrentUser() {
//        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        String login = "mkasd";//todo
        return taskService.findAllDailyPlansForUser(login);
    }

    public void createDailyPlan() {
        if (dailyPlanDate == null) {
            showMessage("Please select date first", FacesMessage.SEVERITY_WARN);
        } else if (DateUtil.isDayBefore(dailyPlanDate)) {
            showMessage("Date should be modern", FacesMessage.SEVERITY_WARN);
        } else {
            String login = "mkasd";//todo
            try {
                taskService.createDailyPlan(dailyPlanDate, login);
                showMessage("Daily plan was created", FacesMessage.SEVERITY_INFO);
            } catch (DuplicatedPlanException e) {
                showMessage(e.getMessage(), FacesMessage.SEVERITY_WARN);
            }
        }
    }

    public String showDailyPlan() {
        return Actions.SUCCESS;
    }

    public void addTaskToDailyPlan() {
        if (newTask.getEndTime().before(newTask.getStartTime())) {
            showMessage("End time should be after start time", FacesMessage.SEVERITY_WARN);
        } else {
            taskService.addTaskToDailyPlan(newTask, selectedDailyPlan);
            showMessage("Task was added successfully", FacesMessage.SEVERITY_INFO);
            newTask = null;
        }
    }

    private void showMessage(String description, FacesMessage.Severity severity) {
        FacesMessage facesMessage = new FacesMessage();
        facesMessage.setSeverity(severity);
        facesMessage.setSummary(description);

        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }
}
