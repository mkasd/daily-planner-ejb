package com.gl.training.dibrivnyi.planner.ui.controller;

import com.gl.training.dibrivnyi.planner.middle.entity.User;
import com.gl.training.dibrivnyi.planner.middle.service.api.AdministrationService;

import javax.enterprise.inject.Default;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Alexandr Dibrivnyi
 *         11.08.14
 *         20:36
 */
@Named
@SessionScoped
public class AddUserController {

    @Inject
    private AdministrationService administrationService;

    private User user;

    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void saveUser() {
        administrationService.addUser(user);
        user = null;

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Successful", "User successfully added"));
    }
}
